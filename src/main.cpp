
#include "board.hpp"
#include <cortexpipi/systick.hpp>



int main() {
  auto& systick = cortexpipi::Systick::instance();
  auto& board = Board::instance();

  for(;;) {
    board.led.toggle();
    systick.delay(board.btn.read()?100:200);
  }

  return 0;
}

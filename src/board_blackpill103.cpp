#include "board.hpp"


#include <cortexpipi/systick.hpp>
#include <libopencm3/stm32/rcc.h>

Board::Board(): led(1,12), btn(0,10)
{
  using Systick = cortexpipi::Systick;
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  auto& systick = Systick::instance();
  systick.init();


  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  led.configure(GpioPin::Mode::OUT);
  btn.configure(GpioPin::Mode::IN);

}

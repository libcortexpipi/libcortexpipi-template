if(
  (NOT DEFINED BOARD)
  OR ("${BOARD}" STREQUAL "")
)
  message(FATAL_ERROR "Board not specified. You should set \"BOARD\" variable")
endif()

if(BOARD STREQUAL blackpill103)
  set(DEVICE stm32f103c8t6)
elseif(BOARD STREQUAL mcudev407)
  set(DEVICE stm32f407vet6)
endif()

if(
  (NOT DEFINED DEVICE)
  OR (DEVICE STREQUAL "")
)
  message(FATAL_ERROR "Unknown board specified! Select the correct one.")
endif()
